import prefab.prelude.config.*
import prefab.tech.pkg.packages.*

import java.nio.file.Paths


@main def main() = deploy { deployment =>

	val opensuseDesktop = deployment.host("opensuse-desktop", userDir=Paths.get("/home/main"))
	val ubuntuDesktop = deployment.host("ubuntu-desktop", userDir=Paths.get("/home/main"))
	val homeServer = deployment.host("home-server", userDir=Paths.get("/root"))

	val desktopHosts = Seq(opensuseDesktop, ubuntuDesktop)


	desktopHosts.foreach(_.assign(Packages.Installed(
		"keepassxc", "unclutter"
	)))

	desktopHosts.foreach(_.assign(Kde()))
	desktopHosts.foreach { host =>
		host.assign(Firefox())
	}

	desktopHosts.foreach(_.assign(Terminal()))
	homeServer.assign(Terminal())

	Seq(opensuseDesktop, homeServer).foreach(_.assign(Syncthing()))
	//opensuseDesktop.assign(JoomlaDevSetup())
}
