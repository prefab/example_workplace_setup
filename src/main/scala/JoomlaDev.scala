import prefab.prelude.config.*
import prefab.prelude.plan.*
import prefab.tech.pkg.packages.*
import prefab.tech.command.CommandAvailable


case class JoomlaDevSetup() extends Plan {
	
	def ensure(host: LocalHost) = {
		
		val dbContainer = "joomla_db"
		val joomlaContainer = "joomla"
		val dbUser = "root"
		val dbPassword = "root" //insecure, only use for dev
		
		if (host.check(CommandAvailable("docker")) == false) {
			host.ensure(Packages.Installed("docker"))
		}
		
		host.ensure(Command(
			"systemctl enable docker --now"
		))
		
		
		val joomlaDir = host.tempDir.dir("joomla")
		
		
		val dbFilesFolder = joomlaDir.dir("db")
		dbFilesFolder.ensure(_.exists())
		
		host.ensure(Command(
			s"docker rm --force $dbContainer"
		))
		
		host.ensure(
			Docker.Run(image="mariadb")
				.name(dbContainer)
				.rm()
				.detach()
				.mountBind(source=dbFilesFolder, target="/var/lib/mysql/")
				.env("MARIADB_ROOT_USER", dbUser)
				.env("MARIADB_ROOT_PASSWORD", dbPassword)
		)
		
		
		val joomlaWebFolder = joomlaDir.dir("web")
		joomlaWebFolder.ensure(_.exists())
		
		host.ensure(Command(
			s"docker rm --force $joomlaContainer"
		))
		
		host.ensure(
			Docker.Run(image="joomla:4.3")
				.name(joomlaContainer)
				.detach()
				.mountBind(source=joomlaWebFolder, target="/var/www/html/")
				.publish(hostPort=8080, containerPort=80)
				.env("JOOMLA_DB_USER", dbUser)
				.env("JOOMLA_DB_PASSWORD", dbPassword)
				.extraArg(s"--link $dbContainer:mysql")
		)
	}
}
