import prefab.prelude.plan.*
import prefab.tech.pkg.packages.*


case class Syncthing() extends Plan {

	def ensure(host: LocalHost) = {
		host.ensure(Packages.Installed("syncthing"))
		
		val homeDir = "/root"
		
		host.file("/etc/systemd/system/syncthing.service")
			.ensure(_.textIs(
		s"""|[Unit]
				|Description=Syncthing - Open Source Continuous File Synchronization
				|Documentation=man:syncthing(1)
				|StartLimitIntervalSec=60
				|StartLimitBurst=4
				|
				|[Service]
				|ExecStart=/usr/bin/syncthing serve --no-browser --no-restart --logflags=0
				|Restart=on-failure
				|RestartSec=1
				|SuccessExitStatus=3 4
				|RestartForceExitStatus=3 4
				|Environment="HOME=$homeDir"
				|
				|# Hardening
				|SystemCallArchitectures=native
				|MemoryDenyWriteExecute=true
				|NoNewPrivileges=true
				|
				|[Install]
				|WantedBy=default.target
				|""".stripMargin
			))
		
		host.ensure(Command(
			"systemctl daemon-reload"
		))
		
		host.ensure(Command(
			"systemctl enable syncthing.service --now"
		))
	}
}
