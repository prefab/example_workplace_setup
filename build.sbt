
ThisBuild / scalaVersion := "3.2.1"

lazy val setup = (project in file("."))

libraryDependencies += "io.prefab" %% "prefab" % "0.1.0-SNAPSHOT"

scalacOptions ++= Seq(
	"-no-indent",
)

